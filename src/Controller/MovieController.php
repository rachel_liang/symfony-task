<?php

namespace App\Controller;

use App\Entity\Movie;
use App\Entity\Director;
use App\Entity\Genre;

use App\Form\Movie1Type;
use App\Form\RatingType;
use App\Repository\MovieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/movie")
 */
class MovieController extends AbstractController
{
    /**
     * @Route("/", name="movie_index", methods={"GET"})
     */
    public function index(MovieRepository $movieRepository): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        // Fetch stuff from the director repository
        $directorRepo = $this->getDoctrine()->getRepository(Director::class);
        $genreRepo = $this->getDoctrine()->getRepository(Genre::class);

        //get contents of the page
        $data = file_get_contents('https://www.eventcinemas.com.au/Movies/GetNowShowing');

        //convert to a PHP array/object
        $array = json_decode($data, true);

        //pulls only the movie section of the array
        $moviearray = $array["Data"]["Movies"];

        //checks each movie against our database
        foreach ($moviearray as $movie) {
            $name = $movie["Name"];
            $directorName = $movie["Director"];
            $genreName = $movie["Genres"];

            //search results for that movie in our database
            $search = $movieRepository->findby(
                ['name' => $name]
            );

            // If we find the movie, it already exists so continue
            if (sizeof($search) > 0) {
                continue;
            } else {
                //search to see if director exists
                $directorsearch = $directorRepo->findby(
                    ['name' => $directorName]
                );

                //didn't find the director
                if (sizeof($directorsearch) == 0) {
                    // If there's no director for the movie
                    if ($directorName == null) {
                        $directorObj = null;
                    } else {
                        //insert the director into the database
                        $directorObj = new Director();
                        $directorObj->setName($directorName);
                        $entityManager->persist($directorObj);
                    }
                    
                } else {
                    $directorObj = $directorsearch[0];
                }
                
                //sanitises genre name
                $sanitisedGenre = strtolower($genreName);
                $sanitisedGenre = ucfirst($sanitisedGenre);

                //search to see if genre exists
                $genresearch = $genreRepo->findby(
                    ['name' => $sanitisedGenre]
                );

                if (sizeof($genresearch) == 0) {
                    //insert the genre into the database
                    $genreObj = new Genre();
                    $genreObj->setName($genreName);
                    $entityManager->persist($genreObj);
                } else {
                    $genreObj = $genresearch[0];
                }

                //make a new movie
                $newMovie = new Movie();
                $newMovie->setName($name);
                $newMovie->setDirector($directorObj);
                $newMovie->addGenre($genreObj);
                $entityManager->persist($newMovie);
                $entityManager->flush();
            } 
        }

        return $this->render('movie/index.html.twig', [            
            'movies' => $movieRepository->findAll()
        ]);
    }

    /**
     * @Route("/new", name="movie_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $movie = new Movie();
        $form = $this->createForm(Movie1Type::class, $movie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($movie);
            $entityManager->flush();

            return $this->redirectToRoute('movie_index');
        }

        return $this->render('movie/new.html.twig', [
            'movie' => $movie,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="movie_show", methods={"GET"})
     */
    public function show(Movie $movie): Response
    {
        return $this->render('movie/show.html.twig', [
            'movie' => $movie,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="movie_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Movie $movie): Response
    {
        $form = $this->createForm(Movie1Type::class, $movie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('movie_index', [
                'id' => $movie->getId(),
            ]);
        }

        return $this->render('movie/edit.html.twig', [
            'movie' => $movie,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/rate", name="movie_rate", methods={"GET","POST"})
     */
    public function rate(Request $request, Movie $movie): Response
    {
        $form = $this->createForm(RatingType::class, $movie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('movie_index', [
                'id' => $movie->getId(),
            ]);
        }

        return $this->render('movie/rate.html.twig', [
            'movie' => $movie,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="movie_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Movie $movie): Response
    {
        if ($this->isCsrfTokenValid('delete'.$movie->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($movie);
            $entityManager->flush();
        }

        return $this->redirectToRoute('movie_index');
    }
}
