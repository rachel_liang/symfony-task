-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: symfonydb
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `director`
--

DROP TABLE IF EXISTS `director`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `director` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `director`
--

LOCK TABLES `director` WRITE;
/*!40000 ALTER TABLE `director` DISABLE KEYS */;
INSERT INTO `director` VALUES (1,'Steven Spielberg'),(2,'Christopher Nolan'),(3,'Quentin Tarantino'),(4,'Simon Kinberg'),(5,'Dexter Fletcher'),(6,'Guy Ritchie'),(7,'Michael Dougherty'),(8,'Trevor Nunn'),(9,'Chad Stahelski'),(10,'Rob Letterman, Benji Samit, Dan Hernandez'),(11,'Anthony Russo, Joe Russo'),(12,'Ali Abbas Zafar'),(13,'Chris Addison'),(14,'David Yarovesky'),(15,'unknown'),(16,'Andy Wachowski'),(17,'LEE Won Tae'),(18,'unknown'),(19,'Alexandre Astier, Louis Clichy'),(20,'Tim Burton'),(21,'Zhang Disha'),(22,'unknown'),(23,'Wayne Blair'),(24,'unknown'),(25,'Damon Gameau'),(26,'Sukh Sanghera'),(27,'unknown'),(28,'Zara Hayes, Celyn Jones'),(29,'unknown'),(30,'unknown'),(31,'unknown'),(32,'Selvaraghavan'),(33,'Simerjit Singh'),(34,'Richard Donner'),(35,'unknown'),(36,'unknown'),(37,'John Hughes'),(38,'unknown'),(39,'unknown'),(40,'unknown');
/*!40000 ALTER TABLE `director` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genre`
--

DROP TABLE IF EXISTS `genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genre`
--

LOCK TABLES `genre` WRITE;
/*!40000 ALTER TABLE `genre` DISABLE KEYS */;
INSERT INTO `genre` VALUES (1,'Fantasy'),(2,'Action'),(3,'Family'),(4,'Comedy'),(5,'action'),(6,'biography'),(7,'adventure'),(8,'action'),(9,'biography'),(10,'action'),(11,'animated'),(12,'action'),(13,'drama'),(14,'comedy'),(15,'horror'),(17,'documentary'),(18,'science fiction'),(19,'action'),(20,'opera'),(21,'adventure'),(22,'family'),(23,'drama'),(24,'animated'),(25,'comedy'),(26,'crime'),(27,'cine india'),(28,'alternate content'),(29,'foreign');
/*!40000 ALTER TABLE `genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20190531031554','2019-05-31 03:17:31'),('20190531053837','2019-05-31 05:39:04'),('20190604050104','2019-06-04 05:01:41');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie`
--

DROP TABLE IF EXISTS `movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `director_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1D5EF26F899FB366` (`director_id`),
  CONSTRAINT `FK_1D5EF26F899FB366` FOREIGN KEY (`director_id`) REFERENCES `director` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie`
--

LOCK TABLES `movie` WRITE;
/*!40000 ALTER TABLE `movie` DISABLE KEYS */;
INSERT INTO `movie` VALUES (1,2,'Inception',5),(2,1,'Big Trouble in Little China',4),(3,4,'X-Men: Dark Phoenix',NULL),(4,5,'Rocketman',NULL),(5,6,'Aladdin',NULL),(6,7,'Godzilla: King of the Monsters',NULL),(7,8,'Red Joan',NULL),(8,9,'John Wick: Chapter 3 - Parabellum',NULL),(9,10,'Pokemon: Detective Pikachu',NULL),(10,11,'Avengers: Endgame',NULL),(11,12,'Bharat',NULL),(12,13,'The Hustle',NULL),(13,14,'Brightburn',NULL),(14,15,'Heavy Water',NULL),(15,16,'The Matrix',NULL),(16,17,'The Gangster, The Cop, The Devil',NULL),(17,18,'ROH Live: Faust',NULL),(18,19,'Asterix: The Secret of the Magic Potion',NULL),(19,20,'Dumbo',NULL),(20,21,'My Best Summer',NULL),(21,22,'Peppa Pig: Festival of Fun',NULL),(22,23,'Top End Wedding',NULL),(23,3,'Pulp Fiction',NULL),(24,24,'Exhibition on Screen: Degas - Passion for Perfection',NULL),(25,25,'2040',NULL),(26,26,'Laiye Je Yaarian',NULL),(27,27,'The Big Bad Fox and Other Tales',NULL),(28,28,'Poms',NULL),(29,29,'Dastaan-E-Miri Piri',NULL),(30,30,'Kolaigaran',NULL),(31,31,'Warren Miller\'s Face of Winter',NULL),(32,32,'NGK - Tamil',NULL),(33,33,'Muklawa',NULL),(34,34,'The Goonies',NULL),(35,35,'Aliens',NULL),(36,36,'Clueless',NULL),(37,37,'Ferris Bueller\'s Day Off',NULL),(38,38,'IFF - My Big Gay Italian Wedding',NULL),(39,39,'Back to the Future Marathon',NULL),(40,40,'Perfect Strangers',NULL);
/*!40000 ALTER TABLE `movie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie_genre`
--

DROP TABLE IF EXISTS `movie_genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie_genre` (
  `movie_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  PRIMARY KEY (`movie_id`,`genre_id`),
  KEY `IDX_FD1229648F93B6FC` (`movie_id`),
  KEY `IDX_FD1229644296D31F` (`genre_id`),
  CONSTRAINT `FK_FD1229644296D31F` FOREIGN KEY (`genre_id`) REFERENCES `genre` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_FD1229648F93B6FC` FOREIGN KEY (`movie_id`) REFERENCES `movie` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie_genre`
--

LOCK TABLES `movie_genre` WRITE;
/*!40000 ALTER TABLE `movie_genre` DISABLE KEYS */;
INSERT INTO `movie_genre` VALUES (1,2),(3,5),(4,6),(5,7),(6,8),(7,9),(8,10),(9,11),(10,12),(11,13),(12,14),(13,15),(14,17),(15,18),(16,19),(17,20),(18,21),(19,22),(20,23),(21,24),(22,25),(23,26),(24,17),(25,17),(26,13),(27,7),(28,4),(29,27),(30,2),(31,28),(32,2),(33,13),(34,7),(35,2),(36,4),(37,4),(38,4),(39,18),(40,29);
/*!40000 ALTER TABLE `movie_genre` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-13 12:46:40
